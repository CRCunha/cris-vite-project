import React, { useEffect, useState } from 'react';
import { Routes, Route, useLocation, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Menu from '../components/menu';
import useStyles from './styles';
import '../App';
import Logo from '../assets/logo.png';
import { background } from '../configs/colors';

// Pages
import Home from '@/pages/home';
import Login from '@/pages/login';
import Scheduler from '@/pages/scheduler';
import Content from '@/pages/contentController';
import Board from '@/pages/board';
import Profile from '@/pages/profile';
import Table from '@/pages/table';

const Router = () => {
  const classes = useStyles();
  const app = useSelector((state) => state.app);
  const [loadding, setLoadding] = useState(true);
  const navigate = useNavigate();

  const setStates = () => {
    setTimeout(() => {
      setLoadding(false);
    }, 1600);
  };

  useEffect(() => {
    setStates();
    navigate(app.activePage);
  }, []);

  let location = useLocation();

  const source = window.location.pathname;

  return (
    <div>
      {loadding ? (
        <div className={classes.App}>
          <img
            id="logoLoadding"
            className={classes.logoLoadding}
            src={Logo}
            alt="Logo"
          />
        </div>
      ) : (
        <div>
          <div
            style={{
              display: 'flex',
              width: '100%',
              backgroundColor: `${background}`,
            }}
          >
            {source !== '/login' ? <Menu /> : null}
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/calendar" element={<Scheduler />} />
              <Route path="/content" element={<Content />} />
              <Route path="/board" element={<Board />} />
              <Route path="/table" element={<Table />} />
              <Route path="/profile" element={<Profile />} />
            </Routes>
          </div>
        </div>
      )}
    </div>
  );
};

export default Router;
