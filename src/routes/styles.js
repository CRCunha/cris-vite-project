import { makeStyles } from '@material-ui/styles';
import { background } from '../configs/colors';

const useStyles = makeStyles(() => ({
  App: {
    height: '100vh',
    backgroundColor: `${background}`,

    display: 'flex',
    justifyContent: 'center',
    alignItems: ' center',
  },

  logoLoadding: {
    width: '5%',
  },
}));

export default useStyles;
