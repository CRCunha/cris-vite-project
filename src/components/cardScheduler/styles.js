import { makeStyles } from '@material-ui/styles';
import { shadow } from '@/configs/colors';

const useStyles = makeStyles({
  cardContainer: {
    width: '100%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    borderBottom: `solid 8px`,
    backgroundColor: '#fff',
    boxShadow: `${shadow}`,
  },
});

export default useStyles;
