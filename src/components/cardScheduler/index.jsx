import useStyles from './styles';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  WeekView,
  Appointments,
} from '@devexpress/dx-react-scheduler-material-ui';
import '../../App.css';
import { motion } from 'framer-motion';

const currentDate = new Date();

const today = new Date();
const dd = String(today.getDate()).padStart(2, '0');
const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!

const yyyy = today.getFullYear();
const todayFix = yyyy + '-' + mm + '-' + dd;

const schedulerData = [
  {
    startDate: `${todayFix}T10:00`,
    endDate: `${todayFix}T11:00`,
    title: 'Meeting',
  },
  {
    startDate: `${todayFix}T12:00`,
    endDate: `${todayFix}T13:30`,
    title: 'Go to a gym',
  },
];

const CardScheduler = ({ color, height }) => {
  const classes = useStyles();

  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        delayChildren: 0.3,
        staggerDirection: -0.3,
      },
    },
  };

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div
        style={{
          width: '95%',
        }}
      >
        <motion.div
          className="container"
          variants={container}
          initial="hidden"
          animate="show"
        >
          <Scheduler data={schedulerData} height={'750'}>
            <ViewState currentDate={currentDate} />
            <WeekView startDayHour={8} endDayHour={15} />
            <Appointments />
          </Scheduler>
        </motion.div>
      </div>
    </div>
  );
};

export default CardScheduler;
