import * as React from 'react';
import { Snackbar, Button } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { closeSnackBar } from '@/redux/appSlice';

const SnackBar = () => {
  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(
      closeSnackBar({
        message: 'Infomações editadas com sucesso',
        open: true,
        severity: 'success',
      }),
    );
  };

  const app = useSelector((state) => state.app);

  const action = (
    <Button
      color="secondary"
      size="small"
      onClick={() =>
        dispatch(
          closeSnackBar({
            message: '',
            open: false,
            severity: 'success',
          }),
        )
      }
    >
      X
    </Button>
  );

  return (
    <div>
      <Snackbar
        //anchorOrigin={('top', 'center')}
        open={app.snackBar.open}
        onClose={handleClose}
        message={app.snackBar.message}
        severity={app.snackBar.severity}
        action={action}
      />
    </div>
  );
};

export default SnackBar;
