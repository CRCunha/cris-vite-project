import { makeStyles } from '@material-ui/styles';
import { purple, primaryFontColor } from '@/configs/colors';
import wallpapper from '../../assets/wp.jpg';

const useStyles = makeStyles({
  mainContainer: {
    display: 'flex',
    minHeight: '100vh',
    borderLeft: `solid 10px ${purple}`,
  },
  menuContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    flexWrap: 'wrap',
    backgroundColor: '#fff',
    padding: '5px 0px',
  },
  imageContainerOpen: {
    display: ' flex',
    justifyContent: 'flex-start',

    marginBottom: 30,
    marginTop: 15,

    '& img': {
      width: 40,
      marginLeft: 20,
      padding: 5,
      //borderLeft: `solid 5px ${purple}`,
      //backgroundColor: `#cccccc1c`,
    },
  },

  imageContainerClose: {
    display: ' flex',
    justifyContent: 'flex-start',

    marginBottom: 30,
    marginTop: 15,

    '& img': {
      width: 40,
      marginLeft: 9,
      padding: 5,
      //borderLeft: `solid 5px ${purple}`,
      //backgroundColor: `#cccccc1c`,
    },
  },
  sideImage: {
    width: 2,
    height: '100%',
    //backgroundImage: `url(${wallpapper})`,
    backgroundColor: '#fff',
    backgroundPositionX: '58%',
    backgroundPositionY: '100%',
    filter: 'brightness(190%)',
  },

  button: {
    color: `${purple}`,
    fontSize: '14pt',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    width: '100%',
    color: `${primaryFontColor}`,
    height: 40,

    display: 'flex',
    justifyContent: 'center',
  },

  iconOpen: {
    color: `${purple}`,
    marginRight: 15,
    fontSize: '14pt',
  },

  iconClose: {
    color: `${purple}`,
    fontSize: '14pt',
  },

  buttonOpen: {
    width: '75%',
    height: '100%',

    padding: '0px 15px',

    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',

    fontWeight: '600',
    fontSize: '12pt',

    cursor: 'pointer',
  },

  buttonClose: {
    width: '100%',
    height: '100%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    fontWeight: '600',
    fontSize: '12pt',

    cursor: 'pointer',
  },
});

export default useStyles;
