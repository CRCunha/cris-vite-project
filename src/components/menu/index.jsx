import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import Logo from '../../assets/logo.png';
import '../../App';
import MenuButton from '../menuButton';
import { useSelector, useDispatch } from 'react-redux';
import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { logout } from '@/redux/userSlice';

const Menu = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const app = useSelector((state) => state.app);
  const [menuSize, setMenuSize] = useState(app.menuOpen ? 250 : 70);

  useEffect(() => {
    if (app.menuOpen) {
      setMenuSize(250);
    } else {
      setMenuSize(70);
    }
  }, [window.innerWidth, app.menuOpen]);

  return (
    <div className={classes.mainContainer}>
      <div
        style={{
          backgroundColor: '#fff',
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'column',
        }}
      >
        <div
          style={{ width: `${menuSize}px` }}
          className={classes.menuContainer}
        >
          <div
            className={clsx(
              app.menuOpen
                ? classes.imageContainerOpen
                : classes.imageContainerClose,
            )}
          >
            <img id="logoLoaddingHover" src={Logo} alt-="logo" />
          </div>
          <MenuButton tittle="Home" icon="home" linkTo="/" />
          <MenuButton tittle="Calendar" icon="calendar" linkTo="/calendar" />
          <MenuButton tittle="Board" icon="clipboard-list" linkTo="/board" />
          <MenuButton tittle="Table" icon="table" linkTo="/table" />
          <MenuButton
            tittle="Content Edit"
            icon="pen-to-square"
            linkTo="/content"
          />
          <MenuButton tittle="Profile" icon="user" linkTo="/profile" />
        </div>
        <div>
          <div className={classes.buttonContainer}>
            <div
              onClick={() => dispatch(logout())}
              className={clsx(
                app.menuOpen ? classes.buttonOpen : classes.buttonClose,
              )}
            >
              <FontAwesomeIcon
                className={clsx(
                  app.menuOpen ? classes.iconOpen : classes.iconClose,
                )}
                icon="power-off"
              />
              {app.menuOpen ? 'Logout' : null}
            </div>
          </div>
          <MenuButton
            tittle=""
            icon={app.menuOpen ? 'angle-left' : 'angle-right'}
          />
        </div>
      </div>

      <div className={classes.sideImage} />
    </div>
  );
};

export default Menu;
