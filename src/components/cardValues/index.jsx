import useStyles from './styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { InputAdornment, Grid, TextField } from '@mui/material';
import { motion } from 'framer-motion';

const cardValues = ({ color, height }) => {
  const classes = useStyles();

  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        delayChildren: 0.3,
        staggerDirection: -0.3,
      },
    },
  };

  return (
    <motion.div
      className="container"
      variants={container}
      initial="hidden"
      animate="show"
    >
      <div
        style={{ borderColor: `${color}`, height: `${height}px` }}
        className={classes.cardContainer}
      ></div>
    </motion.div>
  );
};

export default cardValues;
