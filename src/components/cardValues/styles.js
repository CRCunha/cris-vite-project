import { makeStyles } from '@material-ui/styles';
import { shadow } from '@/configs/colors';

const useStyles = makeStyles({
  cardContainer: {
    width: '100%',

    borderBottom: `solid 8px`,
    backgroundColor: '#fff',
    boxShadow: `${shadow}`,

    marginBottom: 30,
  },
});

export default useStyles;
