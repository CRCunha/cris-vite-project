import React, { useState, useEffect } from 'react';
import useStyles from '../styles';
import { ResponsiveLine } from '@nivo/line';
import data from './data.json';
import { NivoColors } from '@/configs/nivoColors';
import '../../../App.css';

const Line = ({ height }) => {
  const classes = useStyles();
  const [margin, setMargin] = useState({});

  useEffect(() => {
    if (window.innerWidth === 1366) {
      setMargin({ top: 10, right: 55, bottom: 42, left: 20 });
    } else {
      setMargin({ top: 10, right: 10, bottom: 40, left: 35 });
    }
  }, [window.innerWidth]);

  return (
    <div
      style={{ height: `${height}px`, marginLeft: '-30px', marginTop: 15 }}
      className={classes.itemChart}
    >
      <ResponsiveLine
        width={350}
        data={data}
        colors={NivoColors}
        margin={margin}
        xScale={{ type: 'point' }}
        yScale={{
          type: 'linear',
          min: 'auto',
          max: 'auto',
          stacked: true,
          reverse: false,
        }}
        yFormat=" >-.2f"
        curve="cardinal"
        axisTop={null}
        axisRight={null}
        axisBottom={null}
        axisLeft={null}
        enableGridX={false}
        enableGridY={false}
        enableCrosshair={false}
        pointSize={7}
        pointColor={{ theme: 'background' }}
        pointBorderWidth={2}
        pointBorderColor={{ from: 'serieColor' }}
        pointLabelYOffset={-13}
        useMesh={true}
        legends={[]}
      />
    </div>
  );
};

export default Line;
