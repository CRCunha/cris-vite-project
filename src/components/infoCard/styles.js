import { makeStyles } from '@material-ui/styles';
import { shadow } from '@/configs/colors';
import cardBackground from '@/assets/wpCard.png';

const useStyles = makeStyles({
  cardContainer: {
    width: '100%',

    backgroundColor: '#fff',
    boxShadow: `${shadow}`,
    backgroundImage: `url(${cardBackground})`,
    backgroundSize: '100%',
    backgroundPositionY: '25%',

    marginBottom: '-20px',
  },
  item: {
    height: '100%',
    display: 'flex',
    alignItems: 'flex-end',
    '& img': {
      marginLeft: 10,
      height: '118%',
    },
  },
  itemChart: {
    width: 300,
  },
  slider: {
    display: 'flex',
    height: '100%',
    width: '100%',
  },
  sliderItem: {
    height: '100%',
    width: '100%',

    display: 'flex !important',
    justifyContent: ' center',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
});

export default useStyles;
