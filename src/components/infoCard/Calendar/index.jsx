import useStyles from '../styles';
import { ResponsiveTimeRange } from '@nivo/calendar';
import data from './data.json';
import { NivoColors } from '@/configs/nivoColors';
import '../../../App.css';

const Calendar = ({ height }) => {
  const classes = useStyles();

  return (
    <div
      style={{ height: `${height}px`, marginLeft: '-200px', marginTop: 15 }}
      className={classes.itemChart}
    >
      <ResponsiveTimeRange
        data={data}
        width={600}
        height={210}
        from="2022-04-01"
        to="2022-08-12"
        emptyColor="#eeeeee"
        colors={NivoColors}
        margin={{ top: 30, right: 0, bottom: 55, left: 0 }}
        dayBorderWidth={2}
        dayBorderColor="#ffffff"
        legends={[]}
        weekdayTicks={[]}
        labelTextColor={'#3f3f'}
      />
    </div>
  );
};

export default Calendar;
