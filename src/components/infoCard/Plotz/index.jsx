import useStyles from '../styles';
import { ResponsiveSwarmPlot } from '@nivo/swarmplot';
import data from './data.json';
import { NivoColors } from '@/configs/nivoColors';

const Plotz = ({ height }) => {
  const classes = useStyles();

  return (
    <div
      style={{ height: `${height}px`, marginLeft: '-220px' }}
      className={classes.itemChart}
    >
      <ResponsiveSwarmPlot
        data={data}
        width={520}
        height={210}
        enableGridX={false}
        colors={NivoColors}
        groups={['group A', 'group B', 'group C']}
        identity="id"
        value="price"
        valueFormat="$.2f"
        valueScale={{ type: 'linear', min: 0, max: 500, reverse: false }}
        size={{
          key: 'volume',
          values: [4, 20],
          sizes: [6, 20],
        }}
        forceStrength={4}
        simulationIterations={100}
        borderColor={{
          from: 'color',
          modifiers: [
            ['darker', 0.6],
            ['opacity', 0.5],
          ],
        }}
        margin={{ top: 35, right: 80, bottom: 35, left: 80 }}
        axisTop={null}
        axisRight={null}
        axisBottom={null}
        axisLeft={null}
      />
    </div>
  );
};

export default Plotz;
