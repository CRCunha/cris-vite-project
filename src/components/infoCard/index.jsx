import useStyles from './styles';
import { Grid } from '@mui/material';
import Funnel from './Funnel';
import Calendar from './Calendar';
import Plotz from './Plotz';
import Line from './Line';
import Slider from 'react-slick';
import ChartImage from '../../assets/chart.svg';
import CardEdit from '@/components/cardEdit';

const infoCard = ({ height, edit }) => {
  const classes = useStyles();

  const CustonArrow = () => {
    <></>;
  };

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 10000,
    nextArrow: <CustonArrow />,
    prevArrow: <CustonArrow />,
  };

  return (
    <div
      style={{
        height: 295,
        display: 'flex',
        alignItems: 'center',
        marginTop: '-10px',
      }}
    >
      <div className={classes.cardContainer}>
        <Grid
          style={{ height: `${height}px` }}
          justifyContent="space-between"
          container
        >
          <Grid className={classes.item} item xs={8}>
            <img src={ChartImage} alt="chartImage" />
          </Grid>
          <Grid className={classes.itemChart} item xs={4}>
            <Grid
              container
              style={{ height: '100%', paddingLeft: 30 }}
              justifyContent="center"
              alignItems="center"
            >
              <Slider className={classes.slider} {...settings}>
                <div className={classes.sliderItem}>
                  {edit ? (
                    <CardEdit slide={true} height="200" color="#fff" id={2} />
                  ) : (
                    <Funnel height={`${height}`} />
                  )}
                </div>

                <div className={classes.sliderItem}>
                  {edit ? (
                    <CardEdit slide={true} height="200" color="#fff" id={2} />
                  ) : (
                    <Line height={`${height}`} />
                  )}
                </div>
              </Slider>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default infoCard;
