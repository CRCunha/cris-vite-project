import useStyles from '../styles';
import { ResponsiveFunnel } from '@nivo/funnel';
import data from './data.json';
import { NivoColors } from '@/configs/nivoColors';

const Funnel = ({ height }) => {
  const classes = useStyles();

  return (
    <div
      style={{ height: `${height}px`, marginLeft: '-35px' }}
      className={classes.itemChart}
    >
      <ResponsiveFunnel
        data={data}
        colors={NivoColors}
        colorBy="index"
        width={350}
        enableBeforeSeparators={false}
        enableAfterSeparators={false}
        margin={{ top: 20, right: 50, bottom: 20, left: 50 }}
        valueFormat=">-.4s"
        borderWidth={20}
        labelColor={{
          from: 'color',
          modifiers: [['darker', 3]],
        }}
        beforeSeparatorLength={100}
        beforeSeparatorOffset={20}
        afterSeparatorLength={100}
        afterSeparatorOffset={20}
        currentPartSizeExtension={10}
        currentBorderWidth={40}
        motionConfig="wobbly"
      />
    </div>
  );
};

export default Funnel;
