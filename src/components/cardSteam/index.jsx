import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid } from '@mui/material';
import data from './data.json';
import { ResponsiveStream } from '@nivo/stream';
import { NivoColors } from '@/configs/nivoColors';

const CardScater = ({ color, height }) => {
  const classes = useStyles();

  const [dataChart, setDataChart] = useState();

  useEffect(() => {
    setDataChart(data);
  }, [data]);

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid className={classes.title} item xs={12}>
            <div className={classes.text}>Portifolio Value</div>
          </Grid>
          <Grid className={classes.chart} item xs={12}>
            <ResponsiveStream
              data={data}
              keys={['Raoul', 'Josiane', 'Marcel', 'René', 'Paul', 'Jacques']}
              margin={{ top: 40, right: 90, bottom: 70, left: 50 }}
              axisTop={null}
              axisRight={null}
              axisBottom={{
                orient: 'bottom',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: '',
                legendOffset: 36,
              }}
              axisLeft={{
                orient: 'left',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: '',
                legendOffset: -40,
              }}
              enableGridX={true}
              enableGridY={false}
              offsetType="none"
              colors={NivoColors}
              fillOpacity={0.85}
              borderColor={{ theme: 'background' }}
              dotSize={8}
              dotColor={{ from: 'color' }}
              dotBorderWidth={1}
              dotBorderColor={{
                from: 'color',
                modifiers: [['darker', 0.7]],
              }}
              legends={[
                {
                  anchor: 'bottom-right',
                  direction: 'column',
                  translateX: 100,
                  itemWidth: 80,
                  itemHeight: 20,
                  itemTextColor: '#999999',
                  symbolSize: 12,
                  symbolShape: 'circle',
                  effects: [
                    {
                      on: 'hover',
                      style: {
                        itemTextColor: '#000000',
                      },
                    },
                  ],
                },
              ]}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CardScater;
