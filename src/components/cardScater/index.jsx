import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid } from '@mui/material';
import data from './data.json';
import { ResponsiveSwarmPlot } from '@nivo/swarmplot';
import { NivoColors } from '@/configs/nivoColors';

const CardSteam = ({ color, height }) => {
  const classes = useStyles();

  const [dataChart, setDataChart] = useState();

  useEffect(() => {
    setDataChart(data);
  }, [data]);

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid className={classes.title} item xs={12}>
            <div className={classes.text}>Portifolio Value</div>
          </Grid>
          <Grid className={classes.chart} item xs={12}>
            <ResponsiveSwarmPlot
              colors={NivoColors}
              data={dataChart}
              groups={['group A', 'group B', 'group C']}
              identity="id"
              value="price"
              valueFormat="$.2f"
              valueScale={{ type: 'linear', min: 0, max: 500, reverse: false }}
              size={{
                key: 'volume',
                values: [4, 20],
                sizes: [6, 20],
              }}
              forceStrength={4}
              simulationIterations={100}
              borderColor={{
                from: 'color',
                modifiers: [
                  ['darker', 0.6],
                  ['opacity', 0.5],
                ],
              }}
              margin={{ top: 50, right: 60, bottom: 40, left: 60 }}
              axisTop={{}}
              axisRight={{}}
              axisBottom={{}}
              axisLeft={{}}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CardSteam;
