import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Icons from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { openMenu, closeMenu } from '@/redux/appSlice';
import clsx from 'clsx';
import { changeActivePage } from '@/redux/appSlice';

const MenuButton = ({ tittle, icon, linkTo }) => {
  const classes = useStyles();
  const app = useSelector((state) => state.app);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (app.activePage === linkTo) {
      setIsActive(true);
    }
  }, [app.activePage]);

  const handleMenu = () => {
    console.log('menuOpen', app.menuOpen);
    if (app.menuOpen === true) {
      dispatch(closeMenu());
    } else {
      dispatch(openMenu());
    }
  };

  const activePageChange = (link) => {
    setIsActive(false);
    dispatch(changeActivePage(link));
  };

  const iconList = Object.keys(Icons)
    .filter((key) => key !== 'fas' && key !== 'prefix')
    .map((icon) => Icons[icon]);

  library.add(...iconList);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        backgroundColo: 'blue',
      }}
    >
      <div
        onClick={() => {
          linkTo
            ? (navigate(`${linkTo}`), activePageChange(linkTo))
            : handleMenu();
        }}
        className={classes.menuButtonContainer}
      >
        <div
          className={clsx(
            isActive && app.activePage == linkTo
              ? classes.active
              : classes.iactive,
          )}
        ></div>
        <div
          className={clsx(
            app.menuOpen ? classes.buttonOpen : classes.buttonClose,
          )}
        >
          <FontAwesomeIcon
            className={clsx(
              app.menuOpen ? classes.iconOpen : classes.iconClose,
            )}
            icon={`${icon}`}
          />
          {app.menuOpen ? tittle : null}
        </div>
      </div>
    </div>
  );
};

export default MenuButton;
