import { makeStyles } from '@material-ui/styles';
import { primaryFontColor, purple, green, purpleGray } from '@/configs/colors';

const useStyles = makeStyles({
  menuButtonContainer: {
    width: '92%',
    color: `${primaryFontColor}`,
    height: 53,

    display: 'flex',
    justifyContent: 'center',

    marginBottom: 10,
  },
  menuButtonContainerActive: {
    width: '92%',
    color: `${primaryFontColor}`,
    height: 50,

    display: 'flex',
    justifyContent: 'center',

    marginBottom: 10,
    borderLeft: `solid 3px ${purpleGray}`,
    //backgroundImage: `linear-gradient( rgba(0,0,0,0), ${background})`,
  },
  buttonOpen: {
    width: '80%',
    height: '100%',

    padding: '0px 15px',

    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',

    fontWeight: '600',
    fontSize: '12pt',

    cursor: 'pointer',
  },
  active: {
    marginRight: '-10px',
    width: '8px',
    backgroundColor: `${green}`,
  },
  iactive: {
    marginRight: '-10px',
    width: '8px',
    backgroundColor: '#fff',
  },

  buttonClose: {
    width: '100%',
    height: '100%',

    padding: '0px 15px',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    fontWeight: '600',
    fontSize: '12pt',

    cursor: 'pointer',
  },

  iconOpen: {
    color: `${purple}`,
    marginRight: 15,
    fontSize: '15pt',
  },

  iconClose: {
    color: `${purple}`,
    fontSize: '15pt',
  },
});

export default useStyles;
