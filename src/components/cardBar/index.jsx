import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid } from '@mui/material';
import { ResponsiveBar } from '@nivo/bar';
import data from './data.json';
import { NivoColors } from '@/configs/nivoColors';
import { useSelector } from 'react-redux';

const CardBar = ({ color, height }) => {
  const classes = useStyles();
  const app = useSelector((state) => state.app);

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid className={classes.title} item xs={12}>
            <div className={classes.text}>Portifolio Value</div>
          </Grid>
          <Grid className={classes.chart} item xs={12}>
            <ResponsiveBar
              data={data}
              keys={[
                'Value 1',
                'Value 2',
                'Value 3',
                'Value 4',
                'Value 5',
                'Value 6',
              ]}
              indexBy="country"
              margin={{ top: 70, right: 10, bottom: 30, left: 10 }}
              padding={0.7}
              valueScale={{ type: 'linear' }}
              indexScale={{ type: 'band', round: true }}
              colors={NivoColors}
              borderColor={{
                from: 'color',
                modifiers: [['darker', 1]],
              }}
              axisTop={null}
              axisRight={null}
              axisBottom={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: '',
                legendPosition: 'middle',
                legendOffset: 32,
              }}
              axisLeft={null}
              enableGridY={true}
              enableGridX={true}
              enableLabel={false}
              labelSkipWidth={12}
              labelSkipHeight={12}
              labelTextColor={{
                from: 'color',
                modifiers: [['darker', 1.6]],
              }}
              legends={[]}
              role="application"
              ariaLabel="Nivo bar chart demo"
              barAriaLabel={function (e) {
                return (
                  e.id +
                  ': ' +
                  e.formattedValue +
                  ' in country: ' +
                  e.indexValue
                );
              }}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CardBar;
