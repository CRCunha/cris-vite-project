import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid } from '@mui/material';
import data from './data.json';
import { ResponsiveBullet } from '@nivo/bullet';
import { NivoColors, NivoColorsInvert } from '@/configs/nivoColors';

const CardHeat = ({ color, height }) => {
  const classes = useStyles();

  const [dataChart, setDataChart] = useState();

  useEffect(() => {
    setDataChart(data);
  }, [data]);

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid className={classes.title} item xs={12}>
            <div className={classes.text}>Portifolio Value</div>
          </Grid>
          <Grid className={classes.chart} item xs={12}>
            <ResponsiveBullet
              data={dataChart}
              colors={NivoColors}
              measureColors={NivoColorsInvert}
              markerColors={NivoColorsInvert}
              margin={{ top: 50, right: 40, bottom: 70, left: 70 }}
              spacing={46}
              titleAlign="start"
              titleOffsetX={-55}
              measureSize={0.2}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CardHeat;
