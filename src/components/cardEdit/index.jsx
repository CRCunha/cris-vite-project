import React, { useState } from 'react';
import useStyles from './styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import data from '../cardBar/data';
import { useDispatch } from 'react-redux';
import { updateCardBar } from '@/redux/appSlice';
import JSONInput from 'react-json-editor-ajrm';
import locale from 'react-json-editor-ajrm/locale/en';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';
import { openSnackBar } from '@/redux/appSlice';

const cardEdit = ({ color, height, id, slide }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);

  dispatch(updateCardBar(data));

  return (
    <>
      <div
        style={{ borderColor: `${color}`, height: `${height}px` }}
        className={classes.cardContainer}
        onClick={() => {
          slide ? null : setOpen(!open);
        }}
      >
        <FontAwesomeIcon className={classes.icon} icon="pen-to-square" />
      </div>
      {open ? (
        <div className={classes.backDrop}>
          <div className={classes.modalContent}>
            <Typography className={classes.title} variant="subtitle1">
              Edite as informações abaixo
            </Typography>
            <JSONInput
              id="a_unique_id"
              placeholder={data}
              locale={locale}
              confirmGood={false}
              onKeyPressUpdate={true}
              theme={'light_mitsuketa_tribute'}
              width="100%"
              height="670px"
            />
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <div className={classes.buttonContainer}>
                <Button
                  className={classes.btn}
                  onClick={() => setOpen(!open)}
                  variant="contained"
                >
                  Cancelar
                </Button>
                <Button
                  className={classes.btn}
                  onClick={() => {
                    setOpen(!open);
                    dispatch(
                      openSnackBar({
                        message: 'Infomações editadas com sucesso',
                        open: true,
                        severity: 'success',
                      }),
                    );
                  }}
                  variant="contained"
                >
                  Salvar
                </Button>
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default cardEdit;
