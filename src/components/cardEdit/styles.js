import { makeStyles } from '@material-ui/styles';
import {
  shadow,
  purple,
  background,
  primaryFontColorStrong,
} from '@/configs/colors';

const useStyles = makeStyles({
  cardContainer: {
    width: '100%',

    borderBottom: `solid 8px`,
    backgroundColor: '#fff',
    boxShadow: `${shadow}`,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    marginBottom: 30,
    cursor: 'pointer',
  },
  icon: {
    color: `${purple}`,
    fontSize: '38pt',
  },
  modalContent: {
    position: 'fixed',
    top: '10%',
    left: '32%',
    width: 600,
    height: 800,
    padding: 30,
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: `${shadow}`,
  },

  buttonContainer: {
    marginTop: 20,
    display: 'flex',
    justifyContent: 'flex-end',
  },

  button: {
    marginLeft: '20px !important',
    backgroundColor: `${purple} !important`,
  },
  backDrop: {
    zIndex: '10000',
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  title: {
    marginBottom: '30px !important',
    color: `${primaryFontColorStrong}`,
    fontSize: '16pt !important',
  },
  buttonContainer: {
    marginTop: 25,
    width: '40%',
    display: 'flex',
    justifyContent: 'space-between',
  },
  btn: { backgroundColor: `${purple} !important` },
});

export default useStyles;
