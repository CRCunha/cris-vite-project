import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  carrouselContainer: {
    display: 'flex',
    justifyContent: 'center',
    width: '80%',

    padding: '5px 0px',
  },
  slider: {
    width: '100%',
  },

  sliderItem: {
    width: '100%',

    display: 'flex !important',
    justifyContent: ' center',
    alignItems: 'center',
    flexWrap: 'wrap',

    '& img': {
      paddingTop: 10,
      width: '35%',
      backgroundColor: 'none',
    },
  },

  leftImage3: {
    '& img': {
      paddingTop: 10,
      width: '45%',
      backgroundColor: 'none',
    },
  },

  title: {
    marginTop: 55,
    width: '100%',
    color: '#fff',
    fontSize: '28pt',
    fontWeight: '700',
  },
  subTitle: {
    marginTop: 1,
    marginBottom: 25,
    width: '100%',
    color: '#fff',
    fontSize: '12pt',
  },
});

export default useStyles;
