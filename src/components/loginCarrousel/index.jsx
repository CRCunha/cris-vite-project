import React from 'react';
import useStyles from './styles';
import login1 from '@/assets/login1.svg';
import login2 from '@/assets/login2.svg';
import login3 from '@/assets/login3.svg';

import Slider from 'react-slick';

const LoginCarrousel = () => {
  const classes = useStyles();

  const CustonArrow = () => {
    <></>;
  };

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <CustonArrow />,
    prevArrow: <CustonArrow />,
  };

  return (
    <div className={classes.carrouselContainer}>
      <Slider className={classes.slider} {...settings}>
        <div className={classes.sliderItem}>
          <img id="leftImage1" src={login1} />
          <div className={classes.title}>
            Change the Quality
            <br /> of Your Life
          </div>
          <div className={classes.subTitle}>
            Maecenas a ultricies tellus. <br />
            Donec facilisis lorem ante, in condimentum.
          </div>
        </div>

        <div className={classes.sliderItem}>
          <img id="leftImage2" src={login2} />
          <div className={classes.title}>
            Change the Quality
            <br /> of Your Life
          </div>
          <div className={classes.subTitle}>
            Maecenas a ultricies tellus. <br />
            Donec facilisis lorem ante, in condimentum.
          </div>
        </div>

        <div className={classes.sliderItem}>
          <img id="leftImage3" src={login3} />
          <div className={classes.title}>
            Change the Quality
            <br /> of Your Life
          </div>
          <div className={classes.subTitle}>
            Maecenas a ultricies tellus. <br />
            Donec facilisis lorem ante, in condimentum.
          </div>
        </div>
      </Slider>
    </div>
  );
};

export default LoginCarrousel;
