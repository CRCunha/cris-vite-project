import { makeStyles } from '@material-ui/styles';
import { shadow, purple, primaryFontColor } from '@/configs/colors';

const useStyles = makeStyles({
  cardContainer: {
    width: '100%',

    borderBottom: `solid 8px`,
    backgroundColor: '#fff',
    boxShadow: `${shadow}`,

    marginBottom: 30,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: '90%',
    height: '90%',
  },
  title: {
    height: 40,
    borderLeft: `solid 6px ${purple}`,

    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  text: {
    marginLeft: 20,
    fontSize: 22,
    fontHeight: 600,
    color: `${primaryFontColor}`,
  },
  chart: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 450,
  },
});

export default useStyles;
