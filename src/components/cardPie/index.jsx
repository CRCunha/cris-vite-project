import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid } from '@mui/material';
import { ResponsiveRadialBar } from '@nivo/radial-bar';
import data from './data.json';
import { NivoColors } from '@/configs/nivoColors';

const CardPie = ({ color, height }) => {
  const classes = useStyles();
  const [margin, setMargin] = useState({});

  useEffect(() => {
    if (window.innerWidth === 1366) {
      setMargin({ top: 40, right: 30, bottom: 40, left: 30 });
    } else {
      setMargin({ top: 40, right: 70, bottom: 40, left: 70 });
    }
  }, [window.innerWidth]);

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid className={classes.title} item xs={12}>
            <div className={classes.text}>Portifolio Value</div>
          </Grid>
          <Grid className={classes.chart} item xs={12}>
            <ResponsiveRadialBar
              data={data}
              endAngle={309}
              colors={NivoColors}
              valueFormat=" >-.2f"
              padding={0.3}
              cornerRadius={2}
              margin={margin}
              enableCircularGrid={false}
              radialAxisStart={null}
              circularAxisOuter={{
                tickSize: 5,
                tickPadding: 12,
                tickRotation: 0,
              }}
              legends={[]}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CardPie;
