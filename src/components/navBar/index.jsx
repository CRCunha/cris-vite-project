import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  InputAdornment,
  Grid,
  TextField,
  Popover,
  Typography,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import { styled } from '@mui/material/styles';
import { checkNotify } from '@/redux/appSlice';

const NavBar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const app = useSelector((state) => state.app);
  const [height, setHeight] = useState(65);

  const [anchorEl, setAnchorEl] = useState(null);

  const checkMensages = () => {
    setAnchorEl(event.currentTarget);
  };

  function handleClose() {
    setAnchorEl(null);
    dispatch(checkNotify());
  }

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const StyledBadgeAlert = styled(Badge)(({ theme }) => ({
    cursor: 'pointer',
    '& .MuiBadge-badge': {
      backgroundColor: '#ef4c6a',
      color: '#ef4c6a',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: -1,
        left: -1,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: 'ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
  }));

  const StyledBadgeAlertNone = styled(Badge)(({}) => ({
    '& .MuiBadge-badge': {
      backgroundColor: 'rgba(0,0,0,0)',
      color: 'rgba(0,0,0,0)',
    },
  }));

  const StyledBadge = styled(Badge)(({ theme }) => ({
    cursor: 'pointer',
    '& .MuiBadge-badge': {
      backgroundColor: '#A3F78E',
      color: '#A3F78E',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: -1,
        left: -1,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: 'ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
  }));

  useEffect(() => {
    if (window.innerWidth === 1366) {
      setHeight(60);
    } else {
      setHeight(65);
    }
  }, [window.innerWidth]);

  return (
    <div
      style={{ maxHeight: `${height}px` }}
      className={classes.navBarMainContainer}
    >
      <Grid container>
        <Grid
          style={{ maxHeight: `${height}px` }}
          className={classes.gridSideItemBorder}
          item
          xs={4}
        >
          <FontAwesomeIcon className={classes.icon} icon="align-left" />
          <div className={classes.inputContainer}></div>
          <TextField
            fullWidth
            variant="standard"
            sx={{ '& > not(style)': { m: 1, width: '45ch' } }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <FontAwesomeIcon
                    className={classes.searchIcon}
                    icon="magnifying-glass"
                  />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid
          style={{ maxHeight: `${height}px` }}
          className={classes.gridSideItemRight}
          item
          xs={8}
        >
          <div className={classes.icon}>
            {app.notify > 0 ? (
              <div>
                <StyledBadgeAlert
                  overlap="circular"
                  onClick={() => checkMensages()}
                  anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                  variant="dot"
                >
                  <FontAwesomeIcon
                    className={classes.bellIcon}
                    icon="fa-solid fa-bell"
                  />
                </StyledBadgeAlert>

                <Popover
                  style={{ marginTop: 55 }}
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                >
                  <Typography
                    sx={{ p: 2 }}
                  >{`${app.mensages[0].text}`}</Typography>
                </Popover>
              </div>
            ) : (
              <StyledBadgeAlertNone
                overlap="circular"
                anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                variant="dot"
              >
                <FontAwesomeIcon
                  className={classes.bellIcon}
                  icon="fa-solid fa-bell"
                />
              </StyledBadgeAlertNone>
            )}
          </div>
          <StyledBadge
            overlap="circular"
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            variant="dot"
            // onClick={() => dispatch(logout())}
          >
            <Avatar alt={user.name} src="/static/images/avatar/1.jpg" />
          </StyledBadge>
          <div className={classes.icon}>
            <FontAwesomeIcon
              className={classes.downIcon}
              icon="fa-solid fa-chevron-down"
            />
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default NavBar;
