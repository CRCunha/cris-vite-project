import { makeStyles } from '@material-ui/styles';
import { primaryFontColor, purple, shadow } from '@/configs/colors';

const useStyles = makeStyles({
  navBarMainContainer: {
    width: '100%',
    backgroundColor: '#fff',
    boxShadow: `${shadow}`,
    marginBottom: '-25px',
  },
  gridSideItemBorder: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 15,
  },
  gridSideItemRight: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: 15,
    paddingRight: 0,
    borderRight: `solid 10px ${purple}`,
  },
  icon: {
    color: `${primaryFontColor}`,
    marginLeft: 15,
    marginRight: 30,
    fontSize: '14pt',
  },
  bellIcon: {
    color: `${primaryFontColor}`,
    fontSize: '16pt',
  },
  downIcon: {
    color: `${primaryFontColor}`,
    marginLeft: 5,
    fontSize: '10pt',
  },
  searchIcon: {
    color: `${primaryFontColor}`,
    fontSize: '14pt',
  },
});

export default useStyles;
