import useStyles from './styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Grid, Avatar } from '@mui/material';
import data from './data.json';
import { Scrollbars } from 'react-custom-scrollbars';

const cardPersons = ({ color, height }) => {
  const classes = useStyles();

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid item xs={12}>
            <Scrollbars
              autoHide
              autoHideTimeout={1000}
              autoHideDuration={200}
              style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                flexWrap: 'wrap',
                flexGrow: 1,
                height: 450,
              }}
            >
              {data.map((value) => {
                return (
                  <div className={classes.personCard}>
                    <Grid container>
                      <Grid className={classes.item} item xs={10}>
                        <Grid container>
                          <Grid item xs={2}>
                            <Avatar
                              sx={{ width: 50, height: 50 }}
                              alt={value.name}
                              src="/static/images/avatar/1.jpg"
                            />
                          </Grid>
                          <Grid item xs={10}>
                            <Grid style={{ paddingLeft: 10 }} container>
                              <Grid className={classes.name} item xs={12}>
                                {value.name}
                              </Grid>
                              <Grid className={classes.time} item xs={12}>
                                {value.time}
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid className={classes.itemController} item xs={2}>
                        <FontAwesomeIcon
                          className={classes.icon}
                          icon="fa-solid fa-trash-can"
                        />
                      </Grid>
                    </Grid>
                  </div>
                );
              })}
            </Scrollbars>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default cardPersons;
