import { makeStyles } from '@material-ui/styles';
import {
  shadow,
  background,
  purple,
  primaryFontColor,
  primaryFontColorStrong,
} from '@/configs/colors';

const useStyles = makeStyles({
  cardContainer: {
    width: '100%',

    borderBottom: `solid 8px`,
    backgroundColor: `${background}`,

    marginBottom: 30,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: '100%',
    height: 450,
  },
  personsList: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    flexGrow: 1,
    height: 450,
  },
  personCard: {
    width: '100%',
    height: 85,
    backgroundColor: '#fff',
    boxShadow: `${shadow}`,
    marginBottom: 30,
  },
  item: {
    paddingLeft: 20,
    height: 85,
    display: 'flex',
    alignItems: 'center',
  },
  itemController: {
    height: 85,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: `${purple}`,
  },
  icon: {
    color: '#fff',
    fontSize: '14pt',
  },
  name: {
    textAlign: 'start',
    color: `${primaryFontColorStrong}`,
    fontSize: '14pt',
    fontWeight: 600,
  },
  time: {
    textAlign: 'start',
    color: `${primaryFontColor}`,
    fontSize: '10pt',
  },
});

export default useStyles;
