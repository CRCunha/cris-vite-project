import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid } from '@mui/material';
import data from './data.json';
import { ResponsiveHeatMap } from '@nivo/heatmap';
import { NivoColors } from '@/configs/nivoColors';

const CardBullet = ({ color, height }) => {
  const classes = useStyles();

  const [dataChart, setDataChart] = useState();

  useEffect(() => {
    setDataChart(data);
  }, [data]);

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid className={classes.title} item xs={12}>
            <div className={classes.text}>Portifolio Value</div>
          </Grid>
          <Grid className={classes.chart} item xs={12}>
            <ResponsiveHeatMap
              data={dataChart}
              margin={{ top: 30, right: 40, bottom: 80, left: 80 }}
              valueFormat=">-.2s"
              axisTop={null}
              axisRight={null}
              axisLeft={{ tickPadding: 14 }}
              colors={{
                type: 'sequential',
                scheme: 'cool',
              }}
              emptyColor="#fff"
              cellComponent={'circle'}
              legends={[
                {
                  anchor: 'bottom',
                  translateX: 0,
                  translateY: 30,
                  length: 400,
                  thickness: 8,
                  direction: 'row',
                  tickPosition: 'after',
                  tickSize: 3,
                  tickSpacing: 4,
                  tickOverlap: false,
                  tickFormat: '>-.2s',
                  title: 'Value →',
                  titleAlign: 'start',
                  titleOffset: 4,
                },
              ]}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CardBullet;
