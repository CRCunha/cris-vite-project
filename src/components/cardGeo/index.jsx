import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid } from '@mui/material';
import data from './data.json';
import { ResponsiveChoropleth } from '@nivo/geo';
import { NivoColorsHaf } from '@/configs/nivoColors';
import countries from './world_countries.json';

const CardGeo = ({ color, height }) => {
  const classes = useStyles();

  const [dataChart, setDataChart] = useState();

  useEffect(() => {
    setDataChart(data);
  }, [data]);

  return (
    <div
      style={{ borderColor: `${color}`, height: `${height}px` }}
      className={classes.cardContainer}
    >
      <div className={classes.content}>
        <Grid container>
          <Grid className={classes.title} item xs={12}>
            <div className={classes.text}>Portifolio Value</div>
          </Grid>
          <Grid className={classes.chart} item xs={12}>
            <ResponsiveChoropleth
              data={dataChart}
              features={countries.features}
              margin={{ top: 80, right: 0, bottom: 0, left: 0 }}
              colors={NivoColorsHaf}
              domain={[0, 1000000]}
              unknownColor="#fefefe"
              label="properties.name"
              valueFormat=".2s"
              projectionTranslation={[0.84, 0.4]}
              projectionRotation={[0, 0, 0]}
              enableGraticule={false}
              graticuleLineColor="#dddddd"
              borderWidth={0.3}
              projectionScale={170}
              borderColor="#5f5f5f"
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CardGeo;
