import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'app',
  initialState: {
    snackBar: {
      message: '',
      open: false,
      severity: 'success',
    },
    activePage: '/',
    cardBar: [],
    menuOpen: true,
    notify: 1,
    mensages: [
      {
        title: 'Bem vindo(a)!',
        text: 'Finalize seu perfil, preencha suas informações pessoais',
      },
    ],
  },

  reducers: {
    checkNotify(state) {
      return { ...state, notify: 0, mensages: [] };
    },
    addNewNotify(state) {
      return { ...state, notify: 0, mensages: ['nova mensagem'] };
    },
    updateCardBar(state, { payload }) {
      return { ...state, cardBar: payload };
    },
    openSnackBar(state, { payload }) {
      return { ...state, snackBar: payload };
    },
    closeSnackBar(state, { payload }) {
      return { ...state, snackBar: payload };
    },
    openMenu(state) {
      return { ...state, menuOpen: true };
    },
    closeMenu(state) {
      return { ...state, menuOpen: false };
    },
    changeActivePage(state, { payload }) {
      return { ...state, activePage: payload };
    },
  },
});

export const {
  openMenu,
  closeMenu,
  checkNotify,
  addNewNotify,
  updateCardBar,
  openSnackBar,
  closeSnackBar,
  changeActivePage,
} = slice.actions;

export const selectApp = (state) => state.app;

export default slice.reducer;
