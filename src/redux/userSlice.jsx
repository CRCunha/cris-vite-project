import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'user',
  initialState: {
    email: '',
    name: '',
    isLogged: false,
  },
  reducers: {
    changeUser(state, { payload }) {
      return {
        ...state,
        isLogged: true,
        email: payload.email,
        name: payload.name,
      };
    },
    logout(state) {
      return { ...state, isLogged: false, email: '', name: '' };
    },
  },
});

export const { changeUser, logout } = slice.actions;

export const selectUser = (state) => state.user;

export default slice.reducer;
