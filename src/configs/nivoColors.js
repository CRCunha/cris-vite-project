export const NivoColors = [
  '#4FADF6',
  '#00C7FA',
  '#00DDE4',
  '#34EDBB',
  '#A3F78E',
  '#F9F871',
  '#FFD65D',
  '#FFAE6D',
  '#FF8694',
  '#FF6DC7',
  '#9786FF',
];

export const NivoColorsInvert = [
  '#9786FF',
  '#FF6DC7',
  '#FF8694',
  '#FFAE6D',
  '#FFD65D',
  '#F9F871',
  '#A3F78E',
  '#34EDBB',
  '#00DDE4',
  '#00C7FA',
  '#4FADF6',
];

export const NivoColorsHaf = [
  '#4FADF6',
  '#00C7FA',
  '#00DDE4',
  '#34EDBB',
  '#A3F78E',
  '#F9F871',
];
