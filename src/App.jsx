import Router from './routes';
import './App.css';
import error from './assets/404.svg';
import SnackBar from './components/snackBar';

function App() {
  return (
    <>
      <div className="App">
        <Router />
      </div>
      <div className="AppNone">
        <img src={error} id="errorImage" allt="404" />
      </div>
      <SnackBar maxSnack={3} />
    </>
  );
}

export default App;
