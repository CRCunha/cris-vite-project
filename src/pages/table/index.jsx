import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import NavBar from '@/components/navBar';
import useStyles from './styles';
import { Grid } from '@mui/material';
import { Scrollbars } from 'react-custom-scrollbars';
import { useSelector } from 'react-redux';
import Divider from '@/pages/home/divider';

//https://react-table-v7.tanstack.com/docs/examples/material-ui-kitchen-sink

const Table = () => {
  const user = useSelector((state) => state.user);
  const classes = useStyles();
  const navigate = useNavigate();

  useEffect(() => {
    user.isLogged ? null : navigate('/login');
  }, [user]);

  return (
    <div className={classes.boardMainContainer}>
      <div className={classes.boardContent}>
        <NavBar />
        <div className={classes.content}>
          <Scrollbars
            autoHide
            autoHideTimeout={1000}
            autoHideDuration={200}
            style={{ width: '100%', height: '89vh' }}
          >
            <div style={{ marginTop: 20 }} />
            <Divider />
          </Scrollbars>
        </div>
      </div>
    </div>
  );
};

export default Table;
