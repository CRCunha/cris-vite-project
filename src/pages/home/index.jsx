import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import NavBar from '@/components/navBar';
import InfoCard from '@/components/infoCard';
import CardBar from '@/components/cardBar';
import CardPie from '@/components/cardPie';
import CardGeo from '@/components/cardGeo';
import CarSteam from '@/components/cardSteam';
import CardScater from '@/components/cardScater';
import useStyles from './styles';
import { Grid } from '@mui/material';
import { Scrollbars } from 'react-custom-scrollbars';
import Divider from './divider';
import { useSelector } from 'react-redux';
import { motion } from 'framer-motion';

const Home = () => {
  const user = useSelector((state) => state.user);
  const classes = useStyles();
  const navigate = useNavigate();

  useEffect(() => {
    user.isLogged ? navigate('/') : navigate('/login');
  }, [user]);

  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        delayChildren: 0.5,
        staggerDirection: -0.5,
      },
    },
  };

  return (
    <div className={classes.homeMainContainer}>
      <div className={classes.homeContent}>
        <NavBar />
        <div className={classes.content}>
          <Scrollbars
            autoHide
            autoHideTimeout={1000}
            autoHideDuration={200}
            style={{ width: '100%', height: '89vh' }}
          >
            <div style={{ padding: '0px 20px 0px 0px' }}>
              <motion.div
                className="container"
                variants={container}
                initial="hidden"
                animate="show"
              >
                <Grid container spacing={4}>
                  <Grid item xs={12}>
                    <InfoCard height="210" />
                  </Grid>
                </Grid>
              </motion.div>
              <Divider />

              <Grid spacing={4} container>
                <Grid item xs={8}>
                  <motion.div
                    className="container"
                    variants={container}
                    initial="hidden"
                    animate="show"
                  >
                    <CardBar height="550" color="#fff" />
                  </motion.div>
                </Grid>
                <Grid item xs={4}>
                  <motion.div
                    className="container"
                    variants={container}
                    initial="hidden"
                    animate="show"
                  >
                    <CardPie height="550" color="#fff" />
                  </motion.div>
                </Grid>
              </Grid>

              <Grid container item xs={12}>
                <motion.div
                  className="container"
                  variants={container}
                  initial="hidden"
                  animate="show"
                >
                  <CarSteam height="500" color="#fff" />
                </motion.div>
              </Grid>

              <Grid spacing={4} container>
                <Grid item xs={4}>
                  <CardGeo height="550" color="#fff" />
                </Grid>
                <Grid item xs={8}>
                  <CardScater height="550" color="#fff" />
                </Grid>
              </Grid>
            </div>
          </Scrollbars>
        </div>
      </div>
    </div>
  );
};

export default Home;
