import React from 'react';
import CardValues from '@/components/cardValues';
import useStyles from './styles';
import { Grid } from '@mui/material';

const Divider = () => {
  const classes = useStyles();

  return (
    <Grid spacing={4} container>
      <Grid item xs={3}>
        <CardValues height="0" color="#4FADF6" />
      </Grid>
      <Grid item xs={3}>
        <CardValues height="0" color="#A3F78E" />
      </Grid>
      <Grid item xs={3}>
        <CardValues height="0" color="#FFD65D" />
      </Grid>
      <Grid item xs={3}>
        <CardValues height="0" color="#FF8694" />
      </Grid>
    </Grid>
  );
};

export default Divider;
