import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import { Grid, Button, Divider, TextField, Box } from '@mui/material';
import logoBorda from '../../assets/logoBorda.png';
import { useNavigate } from 'react-router-dom';
import LoginCarrousel from '../../components/loginCarrousel';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { changeUser } from '@/redux/userSlice';
import DividerHome from '../home/divider';

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const user = useSelector((state) => state.user);

  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const { handleSubmit, errors, register } = useForm();

  useEffect(() => {
    user.isLogged ? navigate('/') : navigate('/login');
  }, [user]);

  const sendRequest = (formData) => {
    dispatch(changeUser(formData.email));

    user.isLogged ? navigate('/') : navigate('/login');
  };

  const sendDemo = () => {
    dispatch(changeUser({email: 'demo@gmail.com', name: 'Demo'}));

    user.isLogged ? navigate('/') : navigate('/login');
  };

  const onFormError = (err) => {
    const objectErrors = {};
    Object.values(err).map((value) => {
      objectErrors[value.ref.name] = value.message;
      return value;
    });
  };

  return (
    <Box className={classes.App}>
      <Grid className={classes.gridMainContainerDirection} container>
        <Grid item xs={6}>
          <div className={classes.infoContainer}>
            <LoginCarrousel />
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.contentContainer}>
            <img src={logoBorda} className={classes.logo} alt="logoTipo" />
            <Grid container justifyContent="center">
              <Grid item xs={10} className={classes.mediaLogin}>
                <DividerHome />
                <Grid container spacing={2}>
                  <Grid item xs={10}>
                    <Button
                      fullWidth
                      variant="contained"
                      className={classes.googleBtn}
                    >
                      <img
                        src="https://img.icons8.com/bubbles/2x/google-logo.png"
                        alt="google"
                      />
                      Google
                    </Button>
                  </Grid>
                  <Grid item xs={2}>
                    <Button
                      variant="contained"
                      fullWidth
                      className={classes.TtBtn}
                    >
                      <img
                        src="https://help.twitter.com/content/dam/help-twitter/brand/logo.png"
                        alt="tt"
                      />
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={10} className={classes.contentFields}>
                <Grid container>
                  <Grid item xs={12}>
                    <TextField
                      className={classes.fieldText}
                      inputRef={register({
                        required: 'Este campo é obrigatório',
                        pattern: {
                          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i,
                          message: 'Forneça um email válido',
                        },
                      })}
                      inputProps={{ maxLength: 150 }}
                      variant="outlined"
                      required
                      fullWidth
                      disabled={loading}
                      error={!!errors.email}
                      helperText={errors.email?.message || false}
                      name="email"
                      label="E-mail"
                      type="text"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      className={classes.field}
                      inputRef={register({
                        required: 'Este campo é obrigatório',
                      })}
                      inputProps={{ maxLength: 30 }}
                      variant="outlined"
                      required
                      fullWidth
                      disabled={loading}
                      error={!!errors.senha}
                      helperText={errors.senha?.message || false}
                      name="senha"
                      label="Senha"
                      type="password"
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={10}>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Button
                      fullWidth
                      type="submit"
                      variant="contained"
                      disabled={false}
                      onClick={handleSubmit(sendRequest, onFormError)}
                      className={classes.googleBtn}
                    >
                      Register
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      fullWidth
                      type="string"
                      variant="contained"
                      disabled={false}
                      className={classes.googleBtn}
                      onClick={handleSubmit(sendRequest, onFormError)}
                    >
                      Login
                    </Button>
                  </Grid>
                  <Grid item xs={12} className={classes.divider}>
                    <Divider />
                  </Grid>

                  <Grid item xs={12}>
                    <Button
                      fullWidth
                      type="string"
                      variant="contained"
                      disabled={false}
                      className={classes.demoBtn}
                      onClick={() => sendDemo()}
                    >
                      DEMO
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Login;
