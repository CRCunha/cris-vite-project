import { makeStyles } from '@material-ui/styles';
import image from '../../assets/wp.jpg';
import {
  yellow,
  secondaryFontColor,
  green,
  primaryFontColorStrong,
  secondBlue,
} from '@/configs/colors';

const useStyles = makeStyles((theme) => ({
  App: {
    width: '100vw',
    height: '100vh',
    backgroundColor: '#fefefe',
    color: `${primaryFontColorStrong}`,
  },
  infoContainer: {
    height: '100vh',
    backgroundImage: `url(${image})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',

    display: 'flex',
    justifyContent: ' center',
    alignItems: 'center',
  },

  contentContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    padding: '0px 20%',
    height: '100vh',
    '& img': {
      marginBottom: 50,
      width: '30%',
    },
  },
  contentFields: {
    padding: '20px 0px',
  },
  mediaLogin: {
    marginBottom: 30,
  },
  googleBtn: {
    backgroundColor: `${secondBlue} !important`,
    color: '#fff',
    height: '40px !important',
    display: 'flex !important',
    justifyContent: 'center !important',
    alignItems: 'center !important',
    boxShadow: 'none !important',
    '& img': {
      width: 30,
      marginRight: 10,
      marginTop: 48,
    },
  },

  demoBtn: {
    backgroundColor: `${green} !important`,
    color: '#fff',
    height: '40px !important',
    display: 'flex !important',
    justifyContent: 'center !important',
    alignItems: 'center !important',
    boxShadow: 'none !important',
    '& img': {
      width: 30,
      marginRight: 10,
      marginTop: 48,
    },
  },
  TtBtn: {
    backgroundColor: `${secondaryFontColor} !important`,
    display: 'flex',
    height: '40px !important',
    justifyContent: 'center',
    boxShadow: 'none !important',
    '& img': {
      width: 30,
      marginTop: 48,
    },
  },
  fieldText: {
    marginBottom: '20px !important',
  },
  logo: {
    marginTop: '-40px',
  },

  gridMainCOntainerDirection: {
    //[theme.breakpoints.up('600')]: {
    //backgroundColor: 'blue',
    //},
  },
}));

export default useStyles;
