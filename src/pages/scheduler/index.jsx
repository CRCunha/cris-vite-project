import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import NavBar from '@/components/navBar';
import useStyles from './styles';
import { Grid } from '@mui/material';
import { Scrollbars } from 'react-custom-scrollbars';
import { useSelector } from 'react-redux';
import Divider from '../home/divider';

import CardScheduler from '@/components/cardScheduler';

const Home = () => {
  const user = useSelector((state) => state.user);
  const classes = useStyles();
  const navigate = useNavigate();

  useEffect(() => {
    user.isLogged ? null : navigate('/login');
  }, [user]);

  return (
    <div className={classes.homeMainContainer}>
      <div className={classes.homeContent}>
        <NavBar />
        <div className={classes.content}>
          <Scrollbars
            autoHide
            autoHideTimeout={1000}
            autoHideDuration={200}
            style={{ width: '100%', height: '89vh' }}
          >
            <div style={{ marginTop: 20 }} />
            <Divider />
            <div style={{ marginTop: 4 }}>
              <CardScheduler color="#fff" height="820" />
            </div>
          </Scrollbars>
        </div>
      </div>
    </div>
  );
};

export default Home;
