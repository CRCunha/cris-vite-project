import { makeStyles } from '@material-ui/styles';
import { background } from '@/configs/colors';

const useStyles = makeStyles({
  homeMainContainer: {
    minHeight: '100vh',
    textAlign: 'center',
    width: '100%',
    backgroundColor: `${background}`,

    display: ' flex',
    justifyContent: ' center',
    alignItems: 'flex-start',
  },
  homeContent: {
    width: '100%',
    overflow: 'auto',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  content: {
    marginLeft: 15,
    marginTop: 35,
    width: '97%',

    overflow: 'auto',
  },
});

export default useStyles;
