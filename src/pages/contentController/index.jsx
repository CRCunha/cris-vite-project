import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import NavBar from '@/components/navBar';
import useStyles from './styles';
import { Grid } from '@mui/material';
import CardEdit from '@/components/cardEdit';
import { Scrollbars } from 'react-custom-scrollbars';
import { useSelector } from 'react-redux';
import Divider from '../home/divider';
import InfoCard from '@/components/infoCard';

const ContentController = () => {
  const user = useSelector((state) => state.user);
  const classes = useStyles();
  const navigate = useNavigate();

  useEffect(() => {
    user.isLogged ? null : navigate('/login');
  }, [user]);

  return (
    <div className={classes.homeMainContainer}>
      <div className={classes.homeContent}>
        <NavBar />
        <div className={classes.content}>
          <Scrollbars
            autoHide
            autoHideTimeout={1000}
            autoHideDuration={200}
            style={{ width: '100%', height: '89vh' }}
          >
            <div style={{ padding: '20px 20px 0px 0px', marginTop: '-20px' }}>
              <Grid container spacing={4}>
                <Grid item xs={12}>
                  <InfoCard height="210" edit={true} />
                </Grid>
              </Grid>
              <Divider />
              <Grid spacing={4} container>
                <Grid item xs={8}>
                  <CardEdit height="550" color="#fff" id={1} />
                </Grid>
                <Grid item xs={4}>
                  <CardEdit height="550" color="#fff" id={2} />
                </Grid>
              </Grid>

              <Grid container item xs={12}>
                <CardEdit height="500" color="#fff" id={3} />
              </Grid>

              <Grid spacing={4} container>
                <Grid item xs={4}>
                  <CardEdit height="550" color="#fff" id={4} />
                </Grid>
                <Grid item xs={8}>
                  <CardEdit height="550" color="#fff" id={5} />
                </Grid>
              </Grid>
            </div>
          </Scrollbars>
        </div>
      </div>
    </div>
  );
};

export default ContentController;
