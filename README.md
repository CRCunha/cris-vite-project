## Cris-vite-Dash

<p align="center">
<br><br/>
<img align="center" src="https://i.imgur.com/CLpk7dZ.png" width="220">
<br><br/>
<br><br/>
</p>

![License](http://img.shields.io/:license-mit-blue.svg)

**_Plataform Preview Click - (AWS S3) <a href="http://cristianapp.com.br.s3-website-sa-east-1.amazonaws.com/">Production</a>_**

[![Plataform](https://i.imgur.com/Y4Gnw6s.png)]()

The purpose of the platform is to create a fully modular panel to obtain a more aggregated and easier to view data response

**_Overview_**

<img align="center" src="https://i.imgur.com/gpbqPDB.png" width="400">-
<img align="center" src="https://i.imgur.com/vJvAgFA.png" width="400">

<img align="center" src="https://i.imgur.com/9J5ZYy7.png" width="400">-
<img align="center" src="https://i.imgur.com/8U6ABYl.png" width="400">

**_Some Componentes Exemples By: Nivo Charts_**

<img align="center" src="https://i.imgur.com/nDCd5F8.png" width="805">-
<img align="center" src="https://i.imgur.com/dSpT16I.png" width="805">
<img align="center" src="https://i.imgur.com/y0952HE.png" width="400">
<img align="center" src="https://i.imgur.com/BxQlYWr.png" width="400">
<img align="center" src="https://i.imgur.com/dNB0zjR.png" width="805">
